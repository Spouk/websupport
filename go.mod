module gitlab.com/Spouk/websupport

go 1.16

require (
	github.com/fiam/gounidecode v0.0.0-20150629112515-8deddbd03fec
	github.com/mozillazg/go-unidecode v0.1.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)
