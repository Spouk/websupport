//*******************************************************************
// convert string password to hash + compare hash and password eq
//*******************************************************************
package websupport

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"log"
	"time"
)

type hacher struct {
	Thepassword string
}

//encode string-password to hash
func (h *hacher) Encode(password string) (hashpassword string) {
	h.Thepassword = ""
	if hs, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost); err != nil {
		log.Println(err)
		return
	} else {
		hashpassword = string(hs)
		h.Thepassword = hashpassword
	}
	return
}

//compare hash and password in string
func (h *hacher) Checkhashpassword(hashpassword, password string) (bool, error) {
	if err := bcrypt.CompareHashAndPassword([]byte(hashpassword), []byte(password)); err != nil {
		log.Println(err)
		return false, err
	}
	return true, nil
}

//generate new value for cookie
func (h *hacher) CookGenerate(salt string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(time.Now().String()+salt)))
}

//generate hash-list from byte arrays
func (h *hacher) HashSHA(b []byte) string {
	hs := sha1.New()
	hs.Write(b)
	return base64.URLEncoding.EncodeToString(hs.Sum(nil))
}
