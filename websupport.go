//*******************************************************************
// websupport -  stock functions for effort developing web applications with go
// [x] aleksey martynenko aka spouk www.spouk.ru
// https://gitlab.com/Spouk/websupport
// adding tag and version
//*******************************************************************
package websupport

type (
	LoggerWriter interface {
		Printf(format string, a ...any)
		Println(a ...any)
		Fatal(args ...any)
	}

	Websupport struct {
		*convert
		*hacher
		*transliter
		*render
	}
	WebsupportOpt struct {
		RenderPath       string
		RenderDebug      bool
		RenderDebugFatal bool
		Logger           LoggerWriter
	}
)

func New(o WebsupportOpt) Websupport {
	return Websupport{
		convert:    newconverter(o.Logger),
		hacher:     &hacher{},
		transliter: newtranlitter(),
		render: newRender(renderOpt{
			Path:       o.RenderPath,
			Debug:      o.RenderDebug,
			DebugFatal: o.RenderDebugFatal,
			Logger:     o.Logger,
		}),
	}
}
